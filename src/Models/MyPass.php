<?php

namespace Innoractive\MyPassSPWrapper\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MyPass extends Model
{
    use SoftDeletes;

    protected $dateFormat = 'U';

    protected $fillable = [
        'uid', 'user_id', 'name', 'email', 'mobile', 'updated_at', 'deleted_at'
    ];

    protected $dates = ['deleted_at'];

    public function scopeSearchUid($query, $uid){
        return $query->where('uid', $uid);
    }

    public function user(){
        return $this->belongsTo(config('mypass_settings.userModel'));
    }
}
