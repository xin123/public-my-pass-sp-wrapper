<?php

namespace Innoractive\MyPassSPWrapper\Controllers;

use Aacotroneo\Saml2\Events\Saml2LogoutEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Saml2Controller extends Controller {
    public function logout(Request $request) {
        event(new Saml2LogoutEvent(null));
        $request['returnTo'] = route('saml_sls');
        return app('\Aacotroneo\Saml2\Http\Controllers\Saml2Controller')->logout($request);
    }

    public function login() {
        // check saml2_settings.loginRoute if empty
        if (empty(config('saml2_settings.loginRoute'))){
//            config(['saml2_settings.loginRoute' => app('Illuminate\Routing\UrlGenerator')->previous()]);
            config(['saml2_settings.loginRoute' => \Session::get('_previous')['url']]);
        }
        return app('\Aacotroneo\Saml2\Http\Controllers\Saml2Controller')->login();
    }
}