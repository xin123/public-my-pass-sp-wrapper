<?php

namespace Innoractive\MyPassSPWrapper\Events;

abstract class BusMyPassEvent
{
    public $jsonUser;

    public function __construct($jsonUser) {
        $this->jsonUser = $jsonUser;
    }
}
