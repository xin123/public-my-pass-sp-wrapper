<?php

namespace Innoractive\MyPassSPWrapper\Classes;


class HttpPostRequest {
    const TIMEOUT = 5000;

    protected $url;
    protected $fields;
    protected $timeoutMs;
    protected $responseBody;
    protected $responseErrorCode;
    protected $responseErrorMessage;
    protected $responseStatusCode;

    function __construct($url, $fields = [], $timeoutMs = self::TIMEOUT) {
        $this->url = $url;
        $this->fields = $fields;
        $this->timeoutMs = $timeoutMs;

        $this->start();
    }

    protected function start(){
        $ch = curl_init();

        //url-ify the data for the POST
        $fieldsString = '';
        foreach($this->fields as $key=>$value){
            $fieldsString .= $key.'='.urlencode($value).'&';
        }
        rtrim($fieldsString, '&');

        // set option
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $this->timeoutMs);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, count($this->fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsString);

        // get result
        $result = curl_exec($ch);

        $this->responseErrorCode = curl_errno($ch);
        if ($this->responseErrorCode == 0){
            // ok
            $this->responseBody = trim($result);
        }else{
            // error
            $this->responseErrorMessage = curl_error($ch);
        }

        $this->responseStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // override message
        if (!$this->isHttpOK() && $this->responseStatusCode != 0){
            $this->responseErrorMessage = 'HTTP Error';
        }

        //close connection
        curl_close($ch);
    }

    protected function isHttpOK(){
        return ($this->responseStatusCode == 200);
    }

    public function isError(){
        return (!$this->isHttpOK() || ($this->responseErrorCode != 0));
    }

    /**
     * @return string
     */
    public function getResponseBody() {
        return $this->responseBody;
    }

    /**
     * return array
     */
    public function getResponseBodyAsArray() {
        parse_str($this->responseBody, $array);
        return $array;
    }

    /**
     * @return int
     */
    public function getResponseErrorCode() {
        return $this->responseErrorCode;
    }

    /**
     * @return string
     */
    public function getResponseErrorMessage() {
        return $this->responseErrorMessage;
    }

    /**
     * @return int
     */
    public function getResponseStatusCode() {
        return $this->responseStatusCode;
    }
}