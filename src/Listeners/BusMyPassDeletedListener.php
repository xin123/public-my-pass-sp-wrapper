<?php

namespace Innoractive\MyPassSPWrapper\Listeners;

use DB;
use Innoractive\MyPassSPWrapper\Events\BusMyPassDeletedEvent;
use Innoractive\MyPassSPWrapper\Events\MyPassDeletedEvent;
use Innoractive\MyPassSPWrapper\Models\MyPass;

class BusMyPassDeletedListener
{
    public function handle(BusMyPassDeletedEvent $event){
        $jsonUser = $event->jsonUser;

        // check mypass
        $myPass = MyPass::searchUid($jsonUser->uid)->first();
        if (!is_null($myPass)){
            DB::transaction(function () use ($jsonUser, &$myPass) {
                $uid = $myPass->uid;

                $myPass->delete();

                event(new MyPassDeletedEvent($uid));
            });
        }
    }
}
